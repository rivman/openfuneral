## A propos

Ce code source est dérivé de l'article ["Build a Guestbook with Laravel and Vue.js"](https://scotch.io/tutorials/build-a-guestbook-with-laravel-and-vuejs) dont le code est publié
sur le [dépôt suivant](https://github.com/rashidlaasri/build-a-guestbook-with-laravel-and-vuejs.git)

## Configuration / Installation

Copiez le fichier .env.example et modifiez les données importantes

Puis déployez l'application comme toute application laravel !

## Contenu des pages

Sauf cas particulier vous devriez uniquement avoir à modifier les fichiers qui se trouvent dans le dossier resources/views/

## Album photo / relation avec NextCloud

Si vous voulez utiliser la même astuce que moi :

* un partage nextcloud en ecriture seule
* et un autre partage en lecture seule

Ensuite je déplace manuelement les photos d'un dossier à l'autre, ça évite une photo inappropriée au milieu de l'album public par exemple !

Le partage accessible en lecture a un "identifiant" qu'il faut renseigner dans le fichier .env, par exemple

```
APP_PHOTO_NEXTCLOUD_BASEURI="https://xxx.xxxxxxx.ext/"
APP_PHOTO_NEXTCLOUD_PUBLICSHARE="JwHAc4g6P4etdSF"
```

L'application ira se connecter en webdav sur ce partage pour télécharger les photos, les stocker localement et générer des thumbnails.


## Modifications

Si vous modifiez le code source vous devrez probablement relancer la compilation des fichiers de vue.js, ça se fait à l'aide de nodejs:

```
npm install cross-env -g
npm install
npm rebuild node-sass
npm run development
```
