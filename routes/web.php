<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SignaturesController@index')->name('home');

Route::get('list', 'SignaturesController@list')->name('list');
Route::get('flowers', 'SignaturesController@flowers')->name('flowers');
Route::get('bye', 'SignaturesController@bye')->name('bye');

Route::get('sign', 'SignaturesController@create')->name('sign');