<?php

namespace App\Http\Controllers;

class SignaturesController extends Controller
{
    /**
     * Display the GuestBook homepage.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('signatures.index');
    }

    public function list()
    {
        return view('signatures.list');
    }

    public function flowers()
    {
        return view('signatures.flowers');
    }

    public function photo()
    {
        return view('signatures.photo');
    }

    public function bye()
    {
        return view('signatures.bye');
    }

    /**
	 * Display the GuestBook form page.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
	    return view('signatures.sign');
	}
}