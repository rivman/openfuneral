<?php

namespace App\Http\Controllers\Api;

use App\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sabre\DAV\Client;


class PhotoController extends Controller
{
    /**
     * Return a paginated list of photos.
     *
     * @return PhotoResource
     */
    public function index()
    {

        $settings = array(
            'baseUri' => env('APP_PHOTO_NEXTCLOUD_BASEURI'),
            'userName' => env('APP_PHOTO_NEXTCLOUD_PUBLICSHARE'),
            'password' => ''
        );

        $client = new Client($settings);

        $folder_content = $client->propFind('/public.php/webdav/', array(
            '{DAV:}getlastmodified',
            '{DAV:}getcontenttype',
        ),1);

        $fics = array_keys($folder_content);
        for($i = 1; $i< count($fics); $i++) {
            $lignes[] = array('id' => $i,
            'uri' => "/api/photos/show/" . basename($fics[$i]),
            'urifull' => "/album/" . basename($fics[$i])
            );
        }
        $data = array(
            "data" => $lignes,
            "links" => array(
                'first' => App::make('url')->to('/') . "api/photos?page=1",
                'last' =>  App::make('url')->to('/') . "api/photos?page=1",
                'prev' => '',
                'last' => ''
            ),
            "meta" => array(
                'current_page' => 1,
                'from' => 1,
                'last_page' => 1,
                'path' => App::make('url')->to('/') . "api/photos",
                'per_page' => 20,
                'to' => 1,
                'total' => 1
            ),
        );
        return json_encode($data);
    }

}