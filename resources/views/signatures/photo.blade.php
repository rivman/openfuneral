@extends('master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <p>Si vous avez des jolies photos de xxxxx merci de nous <a href="https://votre.nextcloud.ext/uri/depot/seul">les envoyer directement sur le serveur</a> ou par <a href="mailto:xxxxxxxxxxxxxxxxx">courrier électronique</a> ça nous ferait vraiment plaisir ! Attention les photos envoyées sur le serveur sont modérées et seront donc visibles sur <a href="{{ route('photo') }}">l'album public</a> quelques heures après votre dépôt.</p>
    </div>
    <div class="col-md-12">
      <photos></photos>
    </div>
  </div>
</div>
@endsection
