@extends('master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
	<div style="padding: 10px;">
	  <h2>La cérémonie</h2>
	  <div style="float:right; padding-left : 10px;">
	    <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://umap.openstreetmap.fr/fr/map/carte-sans-nom_306344#11/44.7952/-0.6248" style="border: 1px solid black"></iframe>
	  </div>
	  <p class="text-justify">La cérémonie se déroulera le xxxxxxxxxx au Crématorium de Mérignac</p>
	  <ul>
	    <li>Le service civil se déroulera de xxxxxxxxxxxx </li>
	    <li>La crémation aura lieu à xxxxxxxxxx</li>
	    <li>Un livre d'or sera à votre disposition pour laisser vos témoignages d'affection et d'amitié. Si vous voulez laisser votre témoignage en ligne <a href="{{ route('sign') }}">cliquez ici</a></li>
	    <li>N'apportez pas de fleurs, lisez la <a href="{{ route('flowers') }}">page suivante</a></li>
	  </ul>
	  <blockquote>
	    <b>Adresse:</b><br />
	  </blockquote>
	</div>
      </div>
    </div>
  </div>
</div>
@endsection
